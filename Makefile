v ?= 1.15.2
base := root@partidopirata.com.ar:/home/vagancio/

es.json: phony
	vimdiff es.json <(curl "https://framagit.org/les/gancio/-/raw/v$(v)/locales/es.json?ref_type=tags&inline=false")

sync:
	rsync -av $(base)uploads/ ./uploads/
	scp $(base)config.json .
	scp $(base)gancio.sqlite .

phony:
.PHONY: phony
